<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * configuration variable
     * @var Zend_Config_Ini
     */
    protected $_configuration;

    /**
     * front controller variable
     * @var Zend_Controller_Front
     */
    protected $_frontController;

    /**
     * AUTOLOADING
     * @return Zend_Loader_Autoloader
     */
    protected function _initCore()
    {
        Zend_Registry::set('site', new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'site'));
        $autoloader = Zend_Registry::get('Autoloader');
        $autoloader->registerNamespace('My_');

        if (APPLICATION_ENVIRONMENT != 'production')
        {
            $autoloader->suppressNotFoundWarnings(false);
        }
        else
        {
            $autoloader->suppressNotFoundWarnings(true);
        }

        return $autoloader;
    }

    /**
     * CONFIGURATION
     * @return Zend_Config_Ini
     */
    protected function _initConfig()
    {
        $this->_configuration = Zend_Registry::get('Configuration');
        return $this->_configuration;
    }

    /**
     * @throws Zend_Application_Bootstrap_Exception
     */
    protected function _initDb()
    {
        if ($this->hasPluginResource("db"))
        {
            $dbResource = $this->getPluginResource("db");
            $db = $dbResource->getDbAdapter();
            Zend_Registry::set("db", $db);
        }
    }

    /**
     * ERROR LOG
     * to log errors in controllers use the logging action helper
     * @return Zend_Log
     */
    protected function _initLogging()
    {
        $logsDir = APPLICATION_PATH.'/logs/';

        if (!is_dir($logsDir)) @mkdir($logsDir, 0755);

        // init error logger
        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream($logsDir.'application.log');
        $logger->addWriter($writer);

        return $logger;
    }


    /****/
    protected function _initFrontControllerOutput()
    {

        $this->bootstrap('FrontController');
        $frontController = $this->getResource('FrontController');

        $response = new Zend_Controller_Response_Http;
        $response->setHeader('Content-Type', 'text/html; charset=UTF-8', true);
        $frontController->setResponse($response);

        $this->_frontController = $frontController;

        return $frontController;

    }


    /**
     * VIEWS & LAYOUT
     *
     * @return Zend_View
     */
    protected function _initViewHelpers()
    {

        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();
        $view->setEncoding('UTF-8');
        $view->doctype('HTML5');

        /*$websiteTitle = $this->_configuration->website->title;
        $view   ->headTitle()->setSeparator(' - ')
            ->headTitle($websiteTitle);*/

        $view->addHelperPath(APPLICATION_PATH . '/layouts/helpers/', 'Application_Layouts_Helpers');

        return $view;

    }
    /***/
    /**
     * @return Zend_Controller_Router_Interface
     * @throws Zend_Controller_Exception
     */
    protected function _initRoutes()
    {
        //ROUTING
        $this->_frontController->setParam('useDefaultControllerAlways', true);
        $router = $this->_frontController->getRouter();
        $router->removeDefaultRoutes();
        $this->_frontController->setRouter($router);

        return $router;

    }

    protected function _initRouter()
    {
        // Get Router
        $fk = Zend_Controller_Front::getInstance();
        $router = $fk->getRouter();

        $router->addRoute('blogs_index', new Zend_Controller_Router_Route(
            '/',
            array(
                'module'=>'default',
                'controller' => 'blog',
                'action' => 'index'
            )
        ));
        /*$router->addRoute('blogs', new Zend_Controller_Router_Route(
            'blogs/',
            array(
                'module'=>'default',
                'controller' => 'blog',
                'action' => 'index'
            )
        ));*/
        $router->addRoute('blog_name', new Zend_Controller_Router_Route(
            'blog/:name/',
            array(
                'module'=>'default',
                'controller' => 'blog',
                'action' => 'one'
            )
        ));
        $router->addRoute('error', new Zend_Controller_Router_Route(
            'error/',
            array(
                'module'=>'default',
                'controller' => 'error',
                'action' => 'error'
            )
        ));
        $router->addRoute('login', new Zend_Controller_Router_Route(
            'login/',
            array(
                'module'=>'default',
                'controller' => 'login',
                'action' => 'index'
            )
        ));
        $router->addRoute('admin_index', new Zend_Controller_Router_Route(
            'admin/',
            array(
                'module'=>'admin',
                'controller' => 'index',
                'action' => 'index'
            )
        ));
        $router->addRoute('admin_blog_index', new Zend_Controller_Router_Route(
            'admin/blog/',
            array(
                'module'=>'admin',
                'controller' => 'blog',
                'action' => 'index'
            )
        ));
        $router->addRoute('admin_blog_add', new Zend_Controller_Router_Route(
            'admin/blog/add/',
            array(
                'module' => 'admin',
                'controller' => 'blog',
                'action' => 'add'
            )
        ));
        $router->addRoute('admin_login', new Zend_Controller_Router_Route(
            'admin/login/',
            array(
                'module'=>'admin',
                'controller' => 'login',
                'action' => 'index'
            )
        ));
    }
}

