<?php

class BlogController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // Get all posts from DB
        $post = Application_Model_DbTable_Posts::getInstance();
        $this->view->list = $post->getAll();
    }
    public function oneAction()
    {
        // Get all posts from DB
        $post = Application_Model_DbTable_Posts::getInstance();
        try{
            $this->view->post = $post->getPostByURL($this->getParam('name'));
        }catch(Exception $ex){
            throw new Zend_Controller_Action_Exception('Page Not Found!', 404);
        }

    }
}



