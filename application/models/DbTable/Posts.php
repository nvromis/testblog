<?php


class Application_Model_DbTable_Posts extends Zend_Db_Table_Abstract
{

    protected $_name = 'tb_post';
    private static $_instance;

    private function __clone()
    {}

    /**
     * @return Application_Model_DbTable_Posts
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Get single post by id
     * @param $id
     * @return Application_Model_Posts
     */
    public function getPost($id){
        $id = (int)$id;
        $select = $this->select()->from($this->_name)->where('id = ?',$id);
        $row = $this->fetchRow($select);
        if (!row){
            throw new Exception('Not Found!');
        }
        $post = new Application_Model_Posts();
        $post->exchangeArray($row->toArray());

        return $post;
    }

    /**
     * Get single post by url
     * @param $url
     * @return Application_Model_Posts
     * @throws Exception
     */
    public function getPostByURL($url){

        $select = $this->select()->from($this->_name)->where('url_seo = ?',$url);
        $row = $this->fetchRow($select);
        if (!row){
            throw new Exception('Not Found!');
        }
        $post = new Application_Model_Posts();
        $post->exchangeArray($row->toArray());

        return $post;
    }

    /**
     * Get all posts
     * @return array Application_Model_Posts
    */
    public function getAll(){
        $select = $this->select()
            ->from($this->_name)->order('date ASC');

        $row = $this->fetchAll($select);
        $res = array();
        foreach ($row->toArray() as $r) {
            $posts = new Application_Model_Posts();
            $posts->exchangeArray($r);
            $res[]=$posts;
        }
        return $res;
    }

    /**
     * @param Application_Model_Posts $post
     */
    public function createPost(\Application_Model_Posts $post)
    {
        $data = $post->getArrayCopy();
        $this->insert($data);
    }

    /**
     * @param $id
     * @param Application_Model_Posts $post
     */
    public function updatePost($id, \Application_Model_Posts $post)
    {
        $data = $post->getArrayCopy();
        $this->update($data, 'id = ' . (int) $id);
    }

    /**
     * @param $id
     */
    public function deletePost($id)
    {
        $this->delete('id = ' . (int) $id);
    }


}

