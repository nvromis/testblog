<?php
/**
 * User: Roman Nevmerzhitskiy
 * Date: 13.06.2016
 * Time: 12:38
 */

class Application_Model_Posts
{

    public $id;
    public $title;
    public $text_preview;
    public $text_full;
    public $date;
    public $url_seo;
    public $url_img;
    public $id_user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTextPreview()
    {
        return $this->text_preview;
    }

    /**
     * @param mixed $text_preview
     */
    public function setTextPreview($text_preview)
    {
        $this->text_preview = $text_preview;
    }

    /**
     * @return mixed
     */
    public function getTextFull()
    {
        return $this->text_full;
    }

    /**
     * @param mixed $text_full
     */
    public function setTextFull($text_full)
    {
        $this->text_full = $text_full;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getUrlSeo()
    {
        return $this->url_seo;
    }

    /**
     * @param mixed $url_seo
     */
    public function setUrlSeo($url_seo)
    {
        $this->url_seo = $url_seo;
    }

    /**
     * @return mixed
     */
    public function getUrlImg()
    {
        return $this->url_img;
    }

    /**
     * @param mixed $url_img
     */
    public function setUrlImg($url_img)
    {
        $this->url_img = $url_img;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    public function exchangeArray($data){
        foreach ($data as $key=>$val){
            if (property_exists($this, $key)){
                $this->$key = ($val != null) ? $val : null;
            }
        }
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
