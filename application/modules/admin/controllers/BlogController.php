<?php

class Admin_BlogController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function addAction()
    {
        $form = new  Application_Form_Post();
        $news = new Application_Model_Posts();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost();
            //$news->exchangeArray($formData);
            if ($form->isValid($formData)) {
                $news->exchangeArray($formData);
                $db_posts = Application_Model_DbTable_Posts::getInstance();
                $news->setDate(date("Y-m-d H:i:s"));
                /// FIX: ID user from session! NOT '2'.
                $news->setIdUser(2);

                $file = $form->url_img->getFileInfo();
                $ext = explode("[/\\.]", $file['url_img']['name']);
                $newName = $news->getUrlSeo().'.'.$ext[count($ext)-1];
                $path = PUBLIC_PATH.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.$newName;
                $form->url_img->addFilter('Rename', $path);
                $news->setUrlImg('upload'.DIRECTORY_SEPARATOR.$newName);
                $form->url_img->receive();
                $db_posts->createPost($news);
                echo "Successfully added!";
            } else {
                echo "Error ocurred!";
                $form->populate($formData);
                $this->view->form = $form;
            }
        }
        else
        {
            $this->view->form = $form;
        }

    }


}

