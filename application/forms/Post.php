<?php

class Application_Form_Post extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        parent::init();
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $bootstrapStyle = array('class' => 'form-control');

        //$id = new Zend_Form_Element_Hidden('id');

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title')->setAttribs(
            $bootstrapStyle
        );
        $title->setRequired(true);

        $url_seo = new Zend_Form_Element_Text('url_seo');
        $url_seo->setLabel('SEO url')->setAttribs(
            $bootstrapStyle
        );
        $url_seo->setRequired(true);

        $text_preview = new Zend_Form_Element_Textarea('text_preview');
        $text_preview->setLabel('Text preview')->setAttribs(
            array('rows' => 3,
                  'class' => 'form-control')
        );
        $text_preview->setRequired(true);

        $text_full = new Zend_Form_Element_Textarea('text_full');
        $text_full->setLabel('Text full ')->setAttribs(
            array('rows' => 6,
                'class' => 'form-control')
        );
        $text_full->setRequired(true);

        $this->setAttrib('enctype', 'multipart/form-data');

        $url_img = new Zend_Form_Element_File('url_img');

        //FIX:  Need to fix paths for temp img. Not public!
        $url_img->setDestination(PUBLIC_PATH.DIRECTORY_SEPARATOR.'tmp');
        $url_img->setLabel('Image')->setAttribs(
                    $bootstrapStyle)
            ->addValidator('Size', false, 2048000)
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttribs(
            array('class' => 'btn btn-primary btn-block')
        );

       //$this->addElement($id);
        $this->addElement($title);
        $this->addElement($url_seo);
        $this->addElement($url_img);
        $this->addElement($text_preview);
        $this->addElement($text_full);
        $this->addElement($submit);
    }

}

